#
# Makefile for sorting library
#

# compiler selection: prefer clang to gcc
CC = $(shell which clang)
ifeq ($(CC),)
    CC = gcc
endif

ifeq ($(CC),)
    $(warning No compiler found)
    exit 1
endif

# Number of array elements for testing
N_ELEMENTS=100000
# When to switch from n*log(n) to n^2 method (gamasort & sedgesort)
CUTOFF=50

ifeq ($(DEBUG),)
# fast run-time
	CFLAGS = -O3 -fomit-frame-pointer -DCUTOFF=$(CUTOFF)
else
# debugging
	CFLAGS = -ag -DCUTOFF=$(CUTOFF)
endif

SORTOBJ = sorted.o insort.o shellsort.o \
	quicksort.o quickersort.o sedgesort.o heapsort.o

TYPES = float double int8_t int16_t int32_t int64_t \
	uint8_t uint16_t uint32_t uint64_t

.ONESHELL:

.PHONY: all

all:
	for t in $(TYPES); do make $$t; done

sorttest: sorttest.o sort.a
	$(CC) $(CFLAGS) sorttest.o sort.a -o $@ -lm

bstest: bstest.o bsearch.o
	$(CC) $(CFLAGS) bstest.o bsearch.o -o $@

$(SORTOBJ) : %.o : %.c
	$(CC) $(CFLAGS) -c -o $@ $<

.INTERMEDIATE: $(SORTOBJ) _sort.h

lib include:
	mkdir $@

define GEN_RULES_BY_TYPE
$1: lib/sort_$1.a include/sort_$1.h

lib/sort_$1.a: TYPE=$1
lib/sort_$1.a: _sort.h $(SORTOBJ) | lib
	ar rcs $$@ $(filter-out _sort.h,$$^)
endef

$(foreach t,$(TYPES),$(eval $(call GEN_RULES_BY_TYPE,$t)))

include/sort_%.h: | include
	echo "#ifndef sort_$*_h" >> $@
	echo "#define sort_$*_h" >> $@
	echo "#include <stdint.h>" >> $@
	cpp -P sort.h | tail -n10 | sed -e "s/@TYPE@/$*/g" >> $@
	echo "#endif" >> $@

_sort.h:
	sed -e "s/@TYPE@/$(TYPE)/g" sort.h > $@

clean clobber c: .FORCE
	rm -f sorttest bstest *.o *.a
	rm -rf lib include

tar tgz: sort-all.tgz

sort-all.tgz: .FORCE
	cd ..; tar cvf - sort/*.[ch] sort/Makefile sort/gpl.txt | \
		gzip -9 > $@ && mv $@ sort/

.FORCE: ;

test check: all
	@echo === checking sort routines
	@sorttest $(N_ELEMENTS) 99999999 && echo === OK
	@echo === checking binary-search routines
	@bstest 7007 && echo === OK

