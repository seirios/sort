# A library of internal sorting routines
*Ariel Faigon*

From the original webpage http://www.yendor.com/programming/sort/

**Keywords:**
    insertion sort, shellsort, radixsort, heapsort, quicksort, quickersort, sedgesort (Robert Sedgewick quicksort optimization) 

This is a collection of sorting algorithms, in C. All the examples are thoroughly tested using random input generation and assertions, there are no known bugs. I've been using these, especially the fastest *sedgesort*, in production code.

**Licensing:** This code is released under the GNU GPL (General Public License) version 2 which you can find at [gnu.org](http://www.gnu.org/licenses/licenses.html).

The source of these algorithms are classic algorithms and C texts/books which I highly recommend. In some cases I had to convert the original pseudocode to the 'C' programming language to make it actually work (Note: Sedgewick, "Algorithms in C" wasn't yet published when I wrote this code - way back in the 1980's. I used the First edition which used pseudocode for all examples). In addition I have introduced some minor modifications to the code over time to improve style, readability, and efficiency and added a unit-test suite to ensure correctness:

* [Robert Sedgewick: Algorithms in C: Fundamentals, Data Structures, Sorting, Searching](http://www.amazon.com/exec/obidos/ISBN=0201314525/thevanishedgalle/)
* [Jon Louis Bentley: Programming Pearls](http://www.amazon.com/exec/obidos/ISBN=0201103311/thevanishedgalle/)
* [Kernighan & Ritchie: The C Programming Language](http://www.amazon.com/exec/obidos/ISBN=0131103628/thevanishedgalle/)

All the sorting routines are "internal" sorts, i.e. they assume your sorted data is in memory. When sorting big arrays that do not fit in memory on virtual memory systems, performance may suffer significantly.

Certain current implementations of sorting in C++ standard libraries like STL (the HP/SGI Standard Template Library which is now a part of the GNU free software collection) are comparable in speed to *sedgesort*. Note however that the general C/Unix *qsort* routine is much slower due to its generality (e.g. element compare is done via an indirect function call passed as a function pointer.)

Array-size cut-off (CUTOFF) value for switching from recursive to non-recursive:

    /*
     * Interesting historical/technological tidbit:
     *
     * 15 has been found empirically as the optimal cutoff value in 1996
     *
     * 10 years later, in 2006, with computers being very different and
     * much faster, I revisited this constant and found the optimal
     * value to be a close tie between 15 and 16
     *
     * ~7 years later (end of 2013):
     *      - On an Intel i7-4771 with hyper-threading
     *      - Larger caches
     *      - Newer compiler (clang replacing gcc)
     *      - Building with -O3 -fomit-frame-pointer
     * Technology has finally made a difference and could make the switch
     * to a higher CUTOFF value.
     *
     * Here are the run times sorting a 100k element array:
     *  -DCUTOFF=10:    sedgesort:      5148 microsec.
     *  -DCUTOFF=15:    sedgesort:      5032 microsec.
     *  -DCUTOFF=16:    sedgesort:      4998 microsec.
     *  -DCUTOFF=20:    sedgesort:      4926 microsec.
     *  -DCUTOFF=25:    sedgesort:      4880 microsec.
     *  -DCUTOFF=30:    sedgesort:      4830 microsec.
     *  -DCUTOFF=40:    sedgesort:      4770 microsec.
     *  -DCUTOFF=50:    sedgesort:      4746 microsec.  (minima)
     *  -DCUTOFF=60:    sedgesort:      4770 microsec.
     */
    #ifndef CUTOFF
    #  define CUTOFF 50
    #endif
